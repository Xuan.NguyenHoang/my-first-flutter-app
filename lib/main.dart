import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:permission_handler/permission_handler.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;
// import 'package:easy_folder_picker/DirectoryList.dart';
// import 'package:filesystem_picker/filesystem_picker.dart';
// import 'package:file_picker/file_picker.dart';
import 'package:easy_folder_picker/FolderPicker.dart';

import 'package:media_info/media_info.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Easy Folder Picker',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final MediaInfo _mediaInfo = MediaInfo();
  List<FileSystemEntity> files = [];
  List<FileSystemEntity> selectedFiles = [];
  Directory? selectedDirectory;
  Map<String, Map<String, dynamic>> mediaInfosCache = {};
  Map<String, dynamic> mediaInfoCache = {};
  RegExp regexFindKey = RegExp(r'/{(\w+)}/');
  String templateStart = '';
  String templateMid = '';
  String templateEnd = '';
  FileSystemEntity? videoFile;
  String videoFileUrl =
      'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4';
  String videoFilePath = '';

  @override
  void initState() {
    super.initState();
    print('initState');
    _grantPermission();
    _fakeData();
    _loadAsset();
    _loadVideoFromAsset();
  }

  void _grantPermission() async {
    print('_grantPermission');
    var status = await Permission.storage.status;
    if (status.isGranted) {
    } else {
      await Permission.storage.request();
      status = await Permission.storage.status;
      // print(status);
    }
  }

  Future<void> _fakeData() async {
    print('_fakeData');
    // fake data
    var file = File(videoFileUrl);
    files = [file];
    var mediaInfo = await _fetchFileDetail(videoFileUrl);
    mediaInfoCache = mediaInfo;
    mediaInfosCache.putIfAbsent(file.path, () => mediaInfoCache);
    setState(() {
      videoFile = file;
    });
  }

  Future<void> _pickDirectory(BuildContext context) async {
    Directory? directory =
        selectedDirectory ??= Directory(FolderPicker.rootPath);

    Directory? newDirectory = await FolderPicker.pick(
        allowFolderCreation: true,
        context: context,
        rootDirectory: directory,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))));
    setState(() {
      selectedDirectory = newDirectory;
      files = selectedDirectory!
          .listSync(recursive: true)
          .whereType<File>()
          .toList();
      // print(selectedDirectory);
      _fetchFileDetails(files);
    });
  }

  Future<void> _onSelectFile(FileSystemEntity file) async {
    // Thêm file vào danh sách các file được chọn
    if (!selectedFiles.contains(file)) {
      selectedFiles.add(file);
    } else {
      selectedFiles.remove(file);
    }
    // Cập nhật trạng thái của widget
    setState(() {});
  }

  Future<Map<String, dynamic>> _fetchFileDetail(String filePath) async {
    final Map<String, dynamic> mediaInfo =
        await _mediaInfo.getMediaInfo(filePath);
    return mediaInfo;
  }

  Future<void> _fetchFileDetails(List<FileSystemEntity> files) async {
    Map<String, Map<String, dynamic>> mediaInfos = {};
    for (var i = 0; i < files.length; i++) {
      final Map<String, dynamic> mediaInfo =
          await _fetchFileDetail(files[i].path);
      mediaInfos.putIfAbsent(files[i].path, () {
        return mediaInfo;
      });
    }

    setState(() {
      mediaInfosCache = mediaInfos;
    });
  }

  Future<void> _loadAsset() async {
    print('_loadAsset');
    String strStart = await rootBundle.loadString('assets/template_start.txt');
    String strMid = await rootBundle.loadString('assets/template_content.txt');
    String strEnd = await rootBundle.loadString('assets/template_end.txt');

    setState(() {
      templateStart = strStart;
      templateMid = strMid;
      templateEnd = strEnd;
    });
  }

  void _loadVideoFromAsset() async {
    var dir = Directory.systemTemp.createTempSync();
    var temp = File("${dir.path}/video.mp4")..createSync();

    final bytes = await rootBundle.load('assets/video.mp4');
    temp.writeAsBytesSync(
        bytes.buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes));

    final Map<String, dynamic> mediaInfo =
        await _mediaInfo.getMediaInfo(temp.path);
    setState(() {
      videoFilePath = temp.path;
      mediaInfoCache = mediaInfo;
    });
  }

  void _onCreateTextFile(BuildContext context) async {
    Directory saveDirectory = await FolderPicker.pick(
            allowFolderCreation: true,
            context: context,
            rootDirectory: Directory(FolderPicker.rootPath),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)))) ??
        Directory(FolderPicker.rootPath);

    // Tạo file text mới
    File file = File(
      '${saveDirectory.path}/play list.txt',
    );

    // Ghi đường dẫn các file được chọn vào file text
    String content = '$templateStart\n';
    // Iterable<RegExpMatch> matches = regexFindKey.allMatches(strMid);
    // for (final m in matches) {
    //   print(m[0]);
    // }
    for (var file in selectedFiles) {
      content += templateMid
          .toString()
          .replaceAll('{duration}', '6')
          .replaceAll('{filePath}', file.path);
      content += '\n';
    }
    content += '$templateEnd\n';
    await file.writeAsString(content);

    // Thông báo cho người dùng
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('Đã tạo file text thành công!'),
      ),
    );
  }

  void _onReset() {
    // Reset đường dẫn đã chọn
    selectedDirectory = null;
    files = [];

    // Cập nhật trạng thái của widget
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("File List"),
          centerTitle: true,
        ),
        body: Column(children: [
          // Nút chọn thư mục
          ButtonBar(alignment: MainAxisAlignment.center, children: [
            // Icon chọn thư mục
            IconButton(
              icon: const Icon(Icons.folder),
              tooltip: 'Chọn thư mục',
              onPressed: () {
                _pickDirectory(context);
              },
            ),
            // Nút chọn tất cả
            IconButton(
              onPressed: () {
                // Chọn tất cả các file
                selectedFiles = files;

                // Cập nhật trạng thái của widget
                setState(() {});
              },
              tooltip: 'Chọn tất cả',
              icon: const Icon(Icons.select_all),
            ),

            // Nút bỏ chọn tất cả
            IconButton(
              onPressed: () {
                // Bỏ chọn tất cả các file
                selectedFiles = [];

                // Cập nhật trạng thái của widget
                setState(() {});
              },
              tooltip: 'Bỏ chọn tất cả',
              icon: const Icon(Icons.deselect),
            ),
            // Nút reset
            IconButton(
              onPressed: _onReset,
              tooltip: 'reset',
              icon: const Icon(Icons.refresh),
            ),
            // Nút tạo file text
            IconButton(
              onPressed: () {
                _onCreateTextFile(context);
              },
              tooltip: 'Tạo file text',
              icon: const Icon(Icons.edit_note),
            ),
          ]),
          // Thư mục đã chọn
          selectedDirectory != null
              ? Text(
                  "Selected Path : ${selectedDirectory!.path}. There is ${files.length} file(s)")
              : Container(),
          // Danh sách các file
          Expanded(
            child: files.isNotEmpty
                ? Table(
                    // Số hàng trong Table
                    // Tùy chỉnh theo số lượng file
                    children: List.generate(
                      files.length,
                      (index) => TableRow(
                        // Số cột trong Table
                        // Tùy chỉnh theo số cột cần hiển thị
                        children: [
                          // Checkbox để lựa chọn row
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Checkbox(
                              value: selectedFiles.contains(files[index]),
                              onChanged: (value) => _onSelectFile(files[index]),
                            ),
                          ),

                          // Tên file
                          Text(path.basename(files[index].path)),
                          Text(mediaInfoCache['mimeType']),
                          Text(mediaInfoCache['duration']),
                        ],
                      ),
                    ),
                  )
                : const Text('Chưa chọn thư mục'),
          )
        ]));
  }
}
